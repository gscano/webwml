#use wml::debian::template title="Debian 全球鏡像站" BARETITLE=true
#use wml::debian::translation-check translation="c1ef6c3811be792e129656bbfcf09866d9880eb5" maintainer="Kanru Chen"

<p>Debian 正透過數百個伺服器（<em>鏡像</em>）在
網路上[CN:分发:][HKTW:散佈:]。使用離您
較近的伺服器將可能提高下載速度，並且也減輕中央伺服器與全球網路的負擔。</p>

<p class="centerblock">
  许多国家和地区都有 Debian 的镜像站，对于其中一些国家和地区，我们
  添加了类似 <code>ftp.&lt;国家&gt;.debian.org</code> 的别名。
  这个别名通常指向一个能够进行周期性且快速的同步，并且含有 Debian 支持的
  所有架构的镜像站。Debian 仓库总是位于
  该服务器的 <code>/debian</code> 目录，且可通过 <code>HTTP</code> 访问。
</p>

<p class="centerblock">
  其他<strong>镜像站</strong>可能会限制
  它們[CN:鏡像:][HKTW:映射:]的内容（因为空间有限）。
  仅仅因为一个镜像站不是该国家或地区
  的 <code>ftp.&lt;国家&gt;.debian.org</code> 并不意味着它
  比 <code>ftp.&lt;国家&gt;.debian.org</code> 镜像站更慢或内容更旧。
  事实上， 您几乎总是应当优先选择包含您所使用的硬件架构
  且离您更近（所以更快）的镜像站，而非离您更远的镜像站。
</p>

<p>使用離您最近的站台以取得最快的下載速度，不管它是不是国家/地区镜像站别名。
名為
<a href="https://packages.debian.org/stable/net/netselect">
<em>netselect</em></a> 的程式可以用來決定哪個站台有最小的延遲；使用下載程式
如
<a href="https://packages.debian.org/stable/web/wget">
<em>wget</em></a> 或是
<a href="https://packages.debian.org/stable/net/rsync">
<em>rsync</em></a> 來決定哪些站台有最大的輸出頻寬。
注意地理上的接近可能不是決定哪個站台最適合您的最重要因素。</p>

<p>
如果您的机器经常移动，使用基于
全球 <abbr title="内容分发网络">CDN</abbr> 的“镜像”效果可能更好。
Debian 项目将域名 <code>deb.debian.org</code> 用作此种目的，您可以
在 apt 的 sources.list 中使用它 &mdash; 访问\
<a href="http://deb.debian.org/">此服务的网站</a>以获得详细说明。

<p>
其他關於 Debian 鏡像的訊息可以在此獲得：
<url "https://www.debian.org/mirror/">。
</p>

<h2 class="center">Debian 国家/地区镜像站别名</h2>

<table border="0" class="center">
<tr>
  <th>國家/地區</th>
  <th>站台</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Debian 仓库镜像站列表</h2>

<table border="0" class="center">
<tr>
  <th>主機名稱</th>
  <th>HTTP</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
