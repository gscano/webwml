#use wml::debian::template title="Debian 介绍" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian 是一个社区</h2>
      <p>来自世界各地的数以千计的志愿者共同为 Debian 操作系统工作，注重自由和开源软件。认识 Debian 计划。</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">人员：</a>
          我们是谁，我们做什么
        </li>
        <li>
          <a href="philosophy">我们的理念：</a>
          我们为什么要做，我们如何去做
        </li>
        <li>
          <a href="../devel/join/">参与其中：</a>
          如何成为一名 Debian 贡献者
        </li>
        <li>
          <a href="help">作出贡献：</a>
          您如何协助 Debian
        </li>
        <li>
          <a href="../social_contract">Debian 社群契约：</a>
          我们的道德准则
        </li>
        <li>
          <a href="diversity">我们欢迎任何人：</a>
          Debian 多样性声明
        </li>
        <li>
          <a href="../code_of_conduct">致参与者：</a>
          Debian 行为准则
        </li>
        <li>
          <a href="../partners/">合作伙伴：</a>
          为 Debian 计划提供帮助的公司和组织 
        </li>
        <li>
          <a href="../donations">捐赠：</a>
          如何赞助 Debian 计划
        </li>
        <li>
          <a href="../legal/">法律问题：</a>
          协议、商标、隐私政策、专利政策等。
        </li>
        <li>
          <a href="../contact">联系我们：</a>
          如何与我们取得联系
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian 是一个操作系统</h2>
      <p>Debian 是一个自由的操作系统，由 Debian 计划开发和维护。Debian 是一个自由的 Linux 发行版，添加了数以千计的应用程序以满足用户的需要。</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">下载：</a>
          从哪里获取 Debian
        </li>
        <li>
          <a href="why_debian">为什么使用 Debian：</a>
          选择 Debian 的理由
        </li>
        <li>
          <a href="../support">支持：</a>
          从哪里获得帮助
        </li>
        <li>
          <a href="../security">安全：</a>
          最近的更新<br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">软件：</a>
          搜索和浏览 Debian 的众多软件包的列表
        </li>
        <li>
          <a href="../doc">文档：</a>
          安装手册、FAQ、HOWTO、维基，以及更多
        </li>
        <li>
          <a href="../bugs">缺陷追踪系统（BTS）：</a>
          如何报告缺陷、BTS 文档
        </li>
        <li>
          <a href="https://lists.debian.org/">邮件列表：</a>
          面向用户和开发者等群体的 Debian 邮件列表集合
        </li>
        <li>
          <a href="../blends">Pure Blends：</a>
          满足特定需要的元软件包
        </li>
        <li>
          <a href="../devel">开发者天地：</a>
          主要面向 Debian 开发者的信息
        </li>
        <li>
          <a href="../ports">移植/架构：</a>
          Debian 支持的各种 CPU 架构
        </li>
        <li>
          <a href="search">搜索：</a>
          关于如何使用 Debian 搜索引擎的信息
        </li>
        <li>
          <a href="cn">语言：</a>
          Debian 网站的语言设置
        </li>
      </ul>
    </div>
  </div>

</div>

