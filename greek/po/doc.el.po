# Emmanuel Galatoulas <galas@tee.gr>, 2019.
# EG <galatoulas@cti.gr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-08-24 22:01+0300\n"
"Last-Translator: EG <galatoulas@cti.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"  Το Debian 9 είναι το εγχειρίδιο που πρέπει οπωσδήποτε να έχετε για να "
"μάθετε Linux. Ξεκινήστε από το επίπεδο των\n"
"  αρχαρίων και μάθετε πώς να εγκαταστήσετε το σύστημα με  γραφική διεπαφή "
"και τερματικό.\n"
"  Αυτό το βιβλίο παρέχει τις βασικές γνώσεις για να εξελιχθείτε και να "
"γίνετε  'junior'\n"
"  διαχειριστής συστημάτων. Ξεκινήστε εξερευνώντας το περιβάλλον επιφάνειας   "
"εργασίας GNOME\n"
"  και προσαρμόστε το στις προσωπικές σας ανάγκες. Ξεπεράστε τον φόβο σας\n"
"  να χρησιμοποιήσετε το τερματικό του Linux και μάθετε τις πιο σημαντικές "
"εντολές\n"
"  για τη διαχείριση του Debian. Επεκτείνετε τη γνώση σας στις υπηρεσίες του "
"συστήματος (systemd)\n"
"  και μάθετε πώς να τις προσαρμόζετε. Αξιοποιήστε ακόμα περισσότερο το "
"λογισμικό στο Debian και\n"
"  πέρα από το Debian. Διαχειριστείτε το οικιακό σας δίκτυο με το network-"
"manager, κλπ.\n"
"  Ένα ποσοστό 10% από τα κέρδη αυτού του βιβλίου θα δωρηθούν στο Σχέδιο "
"Debian."

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"Γραμμένο από δυο προγραμματιστές του Debian, αυτό το ελεύθερα διαθέσιμο "
"βιβλίο\n"
"  ξεκίνησε ως μια μετάφραση του πετυχημένου βιβλίου τους στα Γαλλικά με τον "
"τίτλο Cahier de\n"
"  l'admin Debian (που έχει εκδοθεί από το Eyrolles). Προσβάσιμο σε όλους, "
"αυτό το βιβλίο\n"
"  διδάσκει τα βασικά σε οποιονδήποτε θέλει να γίνει ένα αποτελεσματικός και\n"
"  ανεξάρτητος διαχειριστής του Debian GNU/Linux.\n"
"  Καλύπτει όλα τα αντικείμενα τα οποία θα πρέπει να κατέχει ένας ικανός "
"διαχειριστής του Linux\n"
"  από την εγκατάσταση και την αναβάθμιση του συστήματος μέχρι\n"
"  τη δημιουργία πακέτων και τη μεταγλώττιση του πυρήνα αλλά και\n"
"  την παρακολούθηση, τη λήψη αντιγράφων ασφαλείας και τη μετεγκατάσταση, "
"χωρίς   να παραλείπει προχωρημένα θέματα\n"
"  όπως τη ρύθμιση του SELinux για την ασφάλεια των υπηρεσιών, τις   "
"αυτοματοποιημένες εγκαταστάσεις ή\n"
"  την εικονοποίηση συστημάτων με χρήση Xen, KVM ή LXC."

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""
"Ο στόχος αυτού του ελέυθερα διαθέσιμου, επικαιροποιημένου βιβλίου είναι να "
"σας κάνει\n"
"  να επιταχύνετε με το Debian. Είναι εμπεριεκτικό όσον αφορά τη βασική "
"υποστήριξη\n"
"  για τον χρήστη που εγκαθιστά και συντηρεί ο ίδιος/η ίδια το σύστημα (είτε\n"
"  στο σπίτι, είτε στο γραφείο, σε μια λέσχη ή στο σχολείο). Κάποιες "
"πληροφορίες συγκεκριμένα για το Debian είναι πολύ παλιές.\n"
"  "

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""
"Το πρώτο βιβλίο στα Γαλλικά για το Debian είναι ήδη στην πέμπτη επανέκδοσή "
"του.\n"
"  Καλύπτει όλες τις πλευρές της διαχείρισης του Debian από την εγκατάσταση\n"
"  μέχρι τη ρύθμιση των δικτυακών υπηρεσιών.\n"
"  Γραμμένο από δύο προγραμματιστές του Debian, αυτό το βιβλίο μπορεί να "
"ενδιαφέρει πολύ\n"
"  κόσμο: τον \"πρωτάρη\" χρήση που θέλει να ανακαλύψει το Debian, τον "
"προχωρημένο χρήση που ψάχνει\n"
"  για συμβουλές ώστε να διευρύνει τη δεξιότητά του στη χρήση των εργαλείων "
"του Debian και\n"
"  τον διαχειριστή που θέλει να φτιάξει ένα αξιόπιστο δίκτυο χρησιμοποιώντας "
"το Debian."

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""
"Το βιβλίο καλύπτει θέματα που ποικίλουν από την έννοια της διαχείρισης "
"πακέτων\n"
"  μέχρι τα διαθέσιμα εργαλεία και το πώς χρησιμοποιούνται σε συγκεκριμένα "
"προβλήματα\n"
"  τα οποία μπορούν να προκύψουν σε πραγματικές συνθήκες και πώς μπορούν να "
"επιλυθούν.   Tο βιβλίο είναι γραμμένο\n"
"  στα Γερμανικά, και μια μετάφραση στα Αγγλικά είνα στα σχέδια. Διαθέσιμο "
"σε: e-book (στο Διαδίκτυο, HTML,\n"
"  PDF, ePub, Mobi), έντυπη έκδοση είναι στα σχέδια.\n"

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""
"Αυτό το βιβλίο σας διδάσκει πώς να εγκαθιστάτε και να ρυθμίζετε το σύστημα "
"καθώς και πώς να χρησιμοποιείτε το Debian σε ένα επαγγελματικό περιβάλλον. "
"Αναδεικνύει τις πλήρεις δυνατότητες της διανομής (στην τρέχουσα έκδοσή της, "
"την 8) και παρέχει ένα πρακτικό εγχειρίδιο για όλους τους χρήστες που θέλουν "
"να μάθουν περισσότερα για το Debian και το φάσμα των υπηρεσιών του."

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""
"Γραμμένο από δύο ερευνητές παραβιάσεων ασφαλείας - Annihilator, Firstblood.\n"
"  Αυτό το βιβλίο σας διδάσκει πώς να φτιάξετε και να ρυθμίσετε έναν "
"εξυπηρετητή  με το Debian 8.x\n"
"   και να ισχυροποιήσετε την ασφάλειά του με τη χρήση της διανομής Kali "
"Linux και του Debian ταυτόχρονα.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 εξυπηρετητής ιστού κλπ.\n"
"  Από την οπτική ότι 'η προέλευση του Kali Linux είναι το Debian', εξηγεί\n"
"  πώς να αυξήσετε την ασφάλεια του Debian εφαρμόζοντας τη μέθοδο του ελέγχου "
"παραβίασης.\n"
"  Αυτό το βιβλίο καλύπτει διάφορα ζητήμα ασφαλείας όπως τα πιστοποιητικά "
"SSL, το τείχος προστασίας UFW,\n"
"  αδυναμίες ασφάλειας της MySQL, εμπρορικά προγράμματα κατά των ιών της "
"Symantec antivirus, συμπεριλαμβανομένου του συστήματος ανίχνευσης "
"παραβιάσεων Snort."

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Συγγραφέας:"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Έκδοση του Debian:"

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "email:"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Διαθέσιμο στο:"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "CD που περιλαμβάνεται:"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Εκδότης:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Συγγραφείς:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Εκδότες:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Συντηρητής:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "Κατάσταση:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Διαθεσιμότητα:"

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "Τελευταία έκδοση:"

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(εκδοση <get-var version />)"

#: ../../english/doc/manuals.defs:131 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "απλό κείμενο"

#: ../../english/doc/manuals.defs:147 ../../english/doc/manuals.defs:157
#: ../../english/doc/manuals.defs:165
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"Ο πιο πρόσφατος <get-var srctype /> πηγαίος κώδικας είναι διαθέσιμος μέσω "
"του αποθετηρίου <a href=\"https://packages.debian.org/git\">Git</a>."

#: ../../english/doc/manuals.defs:149 ../../english/doc/manuals.defs:159
#: ../../english/doc/manuals.defs:167
msgid "Web interface: "
msgstr "Διεπαφή ιστού:"

#: ../../english/doc/manuals.defs:150 ../../english/doc/manuals.defs:160
#: ../../english/doc/manuals.defs:168
msgid "VCS interface: "
msgstr "Διεπαφή VCS: "

#: ../../english/doc/manuals.defs:175 ../../english/doc/manuals.defs:179
msgid "Debian package"
msgstr "Πακέτο του Debian"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package (archived)"
msgstr "Πακέτο Debian (αρχειοθετημένο)"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "λειτουργικό αντίγραφο από τον πηγαίο κώδικα σε CVS: ορίστε το "
#~ "<code>CVSROOT</code>\n"
#~ "  σε <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  καί εξάγετε το άρθρωμα <kbd>boot-floppies/documentation</kbd>."

#~ msgid "CVS via web"
#~ msgstr "CVS μέσω διαδικτύου"

#~ msgid "Language:"
#~ msgstr "Γλώσσα:"

#~ msgid "Latest version in development:"
#~ msgstr "Υπο ανάπτυξη έκδοση:"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a href="
#~ "\"https://packages.debian.org/cvs\">Cvs</a> repository."
#~ msgstr ""
#~ "Ο πιο πρόσφατος <get-var srctype /> πηγαίος κώδικας είναι διαθέσιμος μέσω "
#~ "του αποθετηρίου <a href=\"https://packages.debian.org/cvs\">Cvs</a> ."

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a href="
#~ "\"https://packages.debian.org/subversion\">Subversion</a> repository."
#~ msgstr ""
#~ "Ο πιο πρόσφατος <get-var srctype /> πηγαίος κώδικας είναι διαθέσιμος μέσω "
#~ "του αποθετηρίου <a href=\"https://packages.debian.org/subversion"
#~ "\">Subversion</a>."

#, fuzzy
#~| msgid ""
#~| "Use <a href=\"cvs\">CVS</a> to download the SGML source text for <get-"
#~| "var ddp_pkg_loc />."
#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "Χρησιμοποιήστε <a href=\"cvs\">CVS</a> για κατέβασμα του κώδικα SGML του  "
#~ "<get-var ddp_pkg_loc />."
