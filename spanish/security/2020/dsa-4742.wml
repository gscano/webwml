#use wml::debian::translation-check translation="2f2e4e4ed7b781eff447b99c3d177c672b61e21f"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Tim Starling descubrió dos vulnerabilidades en firejail, un programa
de entorno aislado («sandbox») para restringir el entorno de ejecución de aplicaciones no confiables.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

    <p>Se informó de que firejail no respetaba el marcador de
    fin de opciones ("--"), permitiendo que un atacante con control sobre las opciones
    de la línea de órdenes de la aplicación aislada escribiera datos en un
    fichero especificado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

    <p>Se informó de que firejail, cuando redireccionaba la salida por medio de --output
    o de --output-stderr, concatenaba todos los argumentos de la línea de órdenes en una
    única cadena de caracteres que pasaba a un intérprete de órdenes («shell»). Un atacante que tuviera control
    sobre los argumentos de la línea de órdenes de la aplicación aislada podía
    aprovechar este defecto para ejecutar órdenes arbitrarias.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 0.9.58.2-2+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de firejail.</p>

<p>Para información detallada sobre el estado de seguridad de firejail, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4742.data"
