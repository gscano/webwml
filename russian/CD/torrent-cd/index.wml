#use wml::debian::cdimage title="Загрузка образов компакт-дисков Debian с помощью BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83" maintainer="Lev Lamberov"

<p><a href="https://ru.wikipedia.org/wiki/BitTorrent">BitTorrent</a>&nbsp;&mdash;
это пиринговая система, оптимизированная для большого числа параллельных
загрузок. Её использование снижает нагрузки на наши серверы, поскольку
клиенты BitTorrent по мере загрузки передают части файлов остальным, более
равномерно распределяя таким образом нагрузку по сети и ускоряя загрузку.
</p>

<div class="tip">
<p><strong>Первый</strong> CD/DVD диск содержит все файлы, необходимые для
установки стандартной системы Debian.<br />
Для избежания бесполезных загрузок <strong>не</strong> закачивайте
файлы других образов CD или DVD, если вы не знаете, нужны ли вам находящиеся
на них пакеты.</p>
</div>

<p>
Чтобы загрузить образы CD/DVD Debian этим способом, вам понадобится клиент
BitTorrent. Дистрибутив Debian содержит
<a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> и оригинальные
утилиты <a href="https://packages.debian.org/bittorrent">BitTorrent</a>.
Другие операционные системы поддерживаются следующими клиентами: <a
href="http://www.bittornado.com/download.html">BitTornado</a> и <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>

<h3>Официальные файлы torrent для <q>стабильного</q> выпуска</h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Обязательно просмотрите документацию перед установкой.
<strong>Если вы хотите быстрее начать установку</strong>, прочитайте наше
<a href="$(HOME)/releases/stable/i386/apa">Howto по установке</a>, быстрое
введение в процесс установки. Другая полезная информация:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Руководство по установке</a>,
    детальные инструкции по установке</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Документация по
    Debian-Installer</a>, включает в себя FAQ с общими вопросами и ответами</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, список известных проблем в программе установки</li>
</ul>

#<h3>Официальные файлы torrent для <q>тестируемого</q> дистрибутива</h3>
#
#<ul>
#
#  <li><strong>CD</strong>:<br />
#  <full-cd-torrent>
#  </li>
#
#<ul>
#  <li><strong>DVD</strong>:<br />
#  <full-dvd-torrent>
#  </li>
#
#</ul>

<p>
Если у вас есть возможность, после завершения загрузки не завершайте работу
вашего клиента чтобы помочь остальным загрузить образы быстрее!
</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Если для какого-то оборудования в вашей системе <strong>требуется загрузка несвободных
микропрограмм</strong> вместе с драйвером устройства, то вы можете использовать один из
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tar-архивов распространённых пакетов с микропрограммами</a> или загрузить <strong>неофициальный</strong> образ,
включающий эти <strong>несвободные</strong> микропрограммы. Инструкции о том, как использовать эти tar-архивы,
а также общую информацию о загрузке микропрограмм во время установки можно найти
в <a href="../../releases/stable/amd64/ch06s04">руководстве по установке</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">неофициальные
установочные образы для <q>стабильного</q> выпуска с микропрограммами</a>
</p>
</div>
