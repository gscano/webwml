# translation of webwml/english/po/organization.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <aragorn@tiscali.nl>, 2006.
# Frans Pop <elendil@planet.nl>, 2006, 2007, 2008, 2009, 2010.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml/dutch/po/organization.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-05 22:05+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegatiebericht"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "aanstellingsbericht"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>gemachtigde"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>gemachtigde"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>hij/hem"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>zij/haar"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>gemachtigde"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>zij/hen"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "huidige"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "lid"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "coördinator"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Stabiele-release coördinator"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "grootmeester"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "voorzitter"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretaris"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "vertegenwoordiger"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "functie"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"In de volgende lijst wordt <q>huidige</q> gebruikt voor functies welke\n"
"tijdelijk zijn (verkozen of aangesteld voor bepaalde duur)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Gezagsdragers"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distributie"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Communicatie en doelgroepenwerking"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Gegevensbeschermingsteam"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Publiciteitsteam"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Lidmaatschap van andere organisaties"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:305
msgid "Support and Infrastructure"
msgstr "Ondersteuning en Infrastructuur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Leider"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Technisch comité"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Secretaris"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Ontwikkelingsprojecten"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP-archieven"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP-meesters"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP-assistenten"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP-grootmeesters"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports-team"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Releasemanagement"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Release-team"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Kwaliteitscontrole"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Installatiesysteem-team"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr "Debian team voor live-media"

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Notities bij de uitgave"

#: ../../english/intro/organization.data:153
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB-images"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Productie"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Cloud-team"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Infrastructuur voor geautomatiseerde bouw van pakketten"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Wanna-build-team"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Buildd-beheer"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Documentatie"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Lijst van pakketten waaraan gewerkt moet worden of die eraan komen"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Perscontact"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Webpagina’s"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Doelgroepenwerking"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Vrouwen in Debian"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Gemeenschap"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Om een privébericht te sturen naar alle leden van het Gemeenschapsteam, moet "
"u de GPG-sleutel <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a> gebruiken."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Evenementen"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "Debian conferentiecomité"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Partnerprogramma"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Coördinatie hardwaredonaties"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:292
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:295
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organisatie\n"
"      ter bevordering van gestructureerde informatiestandaarden"

#: ../../english/intro/organization.data:298
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open taal voor\n"
"      het bepalen van kwetsbaarheden"

#: ../../english/intro/organization.data:301
msgid "Open Source Initiative"
msgstr "Open Source Initiative - Openbroninitiatief"

#: ../../english/intro/organization.data:308
msgid "Bug Tracking System"
msgstr "Bugvolgsysteem"

#: ../../english/intro/organization.data:313
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Beheer van mailinglijsten en mailinglijstarchieven"

#: ../../english/intro/organization.data:321
msgid "New Members Front Desk"
msgstr "Aanmeldbalie voor nieuwe leden"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Beheerders van ontwikkelaarsaccounts"

#: ../../english/intro/organization.data:331
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Om een privébericht naar alle beheerders van ontwikkelaarsaccounts te sturen "
"moet u de GPG-sleutel 57731224A9762EA155AB2A530CA8D15BB24D96F2 gebruiken."

#: ../../english/intro/organization.data:332
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Beheerders van encryptiesleutels (PGP en GPG)"

#: ../../english/intro/organization.data:336
msgid "Security Team"
msgstr "Beveiligingsteam"

#: ../../english/intro/organization.data:347
msgid "Policy"
msgstr "Beleid"

#: ../../english/intro/organization.data:350
msgid "System Administration"
msgstr "Systeembeheer"

#: ../../english/intro/organization.data:351
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dit is het adres dat u moet gebruiken als er problemen optreden met een van "
"Debian’s machines, inclusief wachtwoord problemen, of als er een pakket "
"geïnstalleerd moet worden."

#: ../../english/intro/organization.data:361
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Als u hardware problemen hebt met Debian machine, gelieve dan de <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machine</a> pagina te "
"raadplegen. Deze behoort per machine over de nodige beheerdersinformatie te "
"bevatten."

#: ../../english/intro/organization.data:362
msgid "LDAP Developer Directory Administrator"
msgstr "Beheerder van de LDAP-gids van ontwikkelaars"

#: ../../english/intro/organization.data:363
msgid "Mirrors"
msgstr "Spiegelservers"

#: ../../english/intro/organization.data:369
msgid "DNS Maintainer"
msgstr "DNS-beheerder"

#: ../../english/intro/organization.data:370
msgid "Package Tracking System"
msgstr "Pakketvolgsysteem"

#: ../../english/intro/organization.data:372
msgid "Treasurer"
msgstr "Penningmeester"

#: ../../english/intro/organization.data:379
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Aanvragen voor het gebruik van het <a name=\"trademark\" href=\"m4_HOME/"
"trademark\">handelsmerk</a>"

#: ../../english/intro/organization.data:383
msgid "Salsa administrators"
msgstr "Salsa-beheerders"

#~ msgid "CD Vendors Page"
#~ msgstr "Pagina voor CD-verkopers"

#~ msgid "Consultants Page"
#~ msgstr "Pagina met Debian dienstverleners"
