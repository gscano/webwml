<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — een volledige Linuxoplossing voor uw school</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="Frans Spiesschaert"

<p>
Moet u een computerlabo of een volledig schoolnetwerk beheren? Wilt u servers,
werkstations en laptops installeren die onderling samenwerken? Wenst u de
stabiliteit van Debian met reeds vooraf geconfigureerde netwerkdiensten? Wilt u
systemen en verschillende honderden of zelfs meer gebruikersaccounts beheren
met een webtoepassing? Heeft u zich al afgevraagd of en hoe u oudere computers
kunt blijven gebruiken?
</p>

<p>
Dan is Debian Edu iets voor u. Leerkrachten zelf of de technische dienst van de
school kunnen op enkele dagen tijd een volledige leeromgeving ontplooien voor
veel gebruikers en met veel computers. Debian Edu wordt geleverd met honderden
vooraf geïnstalleerde toepassingen, maar u kunt er steeds nog andere pakketten
uit Debian aan toevoegen.
</p>

<p>
Het team van ontwikkelaars van Debian Edu is verheugd Debian Edu 11
<q>Bullseye</q> te kunnen aankondigen, de uitgave van Debian Edu / Skolelinux
die gebaseerd is op de uitgave van Debian 11 <q>Bullseye</q>.
Misschien kunt u deze uitgave uittesten en ons uw bevindingen laten weten
(&lt;debian-edu@lists.debian.org&gt;)
om ons zo te helpen ze nog verder te verbeteren.
</p>

<h2>Over Debian Edu en Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu">Debian Edu, ook bekend als
Skolelinux</a>, is een op Debian gebaseerde Linux-distributie met een
kant-en-klaar en volledig vooraf geconfigureerd schoolnetwerk. Onmiddellijk na
installatie wordt een schoolserver opgezet waarop alle diensten actief zijn die
men in een schoolnetwerk nodig heeft. Er moeten dan nog enkel gebruikers en
computers in het systeem ingevoerd worden via GOsa², een comfortabele
webinterface. Er staat een omgeving ter beschikking om computers over het
netwerk op te starten, waardoor na de initiële installatie van de centrale
server via een cd/dvd/BD of USB-stick, alle andere computers over het netwerk
geïnstalleerd kunnen worden. Oudere computers (zelfs tot ongeveer tien jaar
oud) kunnen gebruikt worden als LTSP thin clients of schijfloze werkstations,
die over het netwerk opstarten en geen enkele installatie of configuratie
behoeven. De schoolserver van Debian Edu gebruikt een LDAP-databank en een
Kerberos authenticatiedienst, heeft gecentraliseerde persoonlijke mappen, een
DHCP-server, een webproxy en vele andere diensten. De grafische werkomgeving
bevat meer dan 70 pakketten met educatieve software en in het Debian-archief
zijn er nog meer te vinden. Scholen kunnen kiezen uit de grafische
werkomgevingen  Xfce, GNOME, LXDE, MATE, KDE Plasma, Cinnamon en LXQt.
</p>

<h2>Nieuwe functionaliteit in Debian Edu 11 <q>Bullseye</q></h2>

<p>Hierna volgen enkele elementen uit de aantekeningen bij de uitgave van
Debian Edu 11 <q>Bullseye</q>, die gebaseerd is op de uitgave van Debian 11
<q>Bullseye</q>. Een volledige lijst met meer gedetailleerde informatie is
opgenomen in het betreffende
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">hoofdstuk</a> van de Handleiding bij Debian Edu, die ook <a href="https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-nl/debian-edu-bullseye-manual.html">in het Nederlands</a>
beschikbaar is.
</p>

<ul>
<li>
Nieuwe <a href="https://ltsp.org">LTSP</a> voor de ondersteuning van schijfloze werkstations. Thin clients worden nog steeds ondersteund, maar nu met de technologie van <a href="https://wiki.x2go.org">X2Go</a>
technology.
</li>
<li>
Opstarten over het netwwerk gebeurt nu met iPXE in plaats van met PXELINUX om
compatibel te zijn met LTSP.
</li>
<li>
Voor iPXE-installaties wordt de grafische modus vah het installatiesysteem van
Debian gebruikt.
</li>
<li>
Samba wordt nu geconfigureerd als <q>autonome server</q> met ondersteuning voor
SMB2/SMB3.
</li>
<li>
DuckDuckGo wordt gebruikt als standaard zoekmachine voor zowel Firefox ESR als
Chromium.
</li>
<li>
Er werd een nieuw hulpmiddel toegevoegd om freeRADIUS in te stellen met
ondersteuning voor zowel EAP-TTLS/PAP als PEAP-MSCHAPV2 als verificatiemethode.
</li>
<li>
Er is een verbeterd hulpmiddel beschikbaar om een nieuw systeem met het profiel
<q>Minimaal</q> specifiek te configureren als gateway.
</li>
</ul>

<h2>Downloadmogelijkheden, installatiestappen en handleiding</h2>

<p>
Officiële Debian netwerkinstallatie-cd-images voor 64-bits en 32-bits pc's zijn
beschikbaar. Slechts in uitzonderlijke situaties (voor pc's die ouder zijn dan
15 jaar) zal het 32-bits image nodig zijn. De images kunnen gedownload worden
van de volgende locaties:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Er zijn eveneens officiële Debian BD-images (meer dan 5 GB) beschikbaar. Het is
hiermee mogelijk om een volledig Debian Edu netwerk in te stellen zonder
toegang tot het internet (met alle ondersteunde grafische werkomgevingen en
alle door Debian ondersteunde talen). Deze images kunnen gedownload worden van
de volgende locaties:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
De images kunnen geverifieerd worden aan de hand van de ondertekende
controlegetallen die in de downloadmap ter beschikking gesteld worden.
<br />
Na het downloaden van een image, kunt u controleren
</p>
<ul>
<li>
of het controlegetal ervan overeenkomt met het volgens het controlegetalbestand
verwachte controlegetal;
</li>
<li>
en of er niet geknoeid werd met het controlegetalbestand.
</li>
</ul>
<p>
Meer informatie over het uitvoeren van deze controlestappen vindt u in het
<a href="https://www.debian.org/CD/verify">verificatievademecum</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> is volledig gebaseerd op Debian 11
<q>Bullseye</q>. Bijgevolg is de broncode van alle pakketten beschikbaar in het
archief van Debian.
</p>

<p>
Raadpleeg ook de
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">Debian Edu Bullseye
statuspagina</a>.
voor steeds up-to-date informatie over Debian Edu 11 <q>Bullseye</q> en voor
instructies over het gebruik van <code>rsync</code> om de ISO-images te
downloaden.
</p>

<p>
Als u opwaardeert van Debian Edu 10 <q>Buster</q>, lees dan het betreffende
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">hoofdstuk uit de handleiding voor Debian Edu</a>.
</p>

<p>
Lees voor installatie-informatie het betreffende
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">hoofdstuk uit de handleiding voor Debian Edu</a>.
</p>

<p>
Na installatie moet u deze
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">eerste handelingen</a> stellen.
</p>

<p>
Raadpleeg de <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">Debian Edu wiki pagina's</a>
voor de recentste Engelstalige versie van de handleiding voor Debian Edu
<q>Bullseye</q>.
De handleiding werd volledig vertaald naar het Duits, het Frans, het Italiaans,
het Deens, het Nederlands, het Noorse Bokmål, het Japans, het vereenvoudigd
Chinees en het Protugees (Portugal). In het Spaans, het
Roemeens en het Pools is er een gedeeltelijke vertaling beschikbaar.
Er bestaat ook een overzicht van <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
de recentste vertaalde versies van de handleiding</a>.
</p>

<p>
Meer informatie over Debian 11 <q>Bullseye</q> zelf wordt gegeven in de
notities bij de release en de installatiehandleiding;
zie <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Over Debian</h2>

<p>>Het Debian Project is een samenwerkingsverband van ontwikkelaars van Vrije
Software die op vrijwillige basis tijd en energie investeren in het
vervaardigen van het volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Raadpleeg voor bijkomende informatie de Debian webpagina's op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail (in het
Engels) naar
&lt;press@debian.org&gt;.</p>

