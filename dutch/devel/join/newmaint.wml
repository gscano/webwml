#use wml::debian::template title="Hoekje voor nieuwe leden van Debian" BARETITLE="true"
#use wml::debian::translation-check translation="e75c4ef4f01457261f11a81e80de9862be35be30"

<p>Het proces voor nieuwe leden van Debian is het proces waarbij men een
officiële ontwikkelaar van Debian wordt (in het Engels Debian Developper - DD).
Op deze pagina's kunnen toekomstige ontwikkelaars van Debian alle informatie
vinden over het indienen van de vraag om een ontwikkelaar van Debian te worden,
over de verschillende stappen in het proces en over hoe men het proces van zijn
lopende kandidatuur kan opvolgen.</p>

<p>Het eerste belangrijke punt dat verduidelijkt moet worden, is dat u
<em>geen</em> officiële ontwikkelaar van Debian moet zijn om te helpen
bij het verbeteren van Debian. In feite zou u al een staat van dienst
van eerdere bijdragen aan Debian moeten hebben voordat u zich aanmeldt
voor het proces voor nieuwe leden.</p>

<p><a name="non-maintainer-contributions"></a>Debian is een open gemeenschap en
verwelkomt iedereen die onze distributie wenst te gebruiken of te verbeteren.
Als niet-ontwikkelaar kunt u:</p>

<ul>
  <li>pakketten onderhouden via een <a href="#Sponsor">sponsor</a></li>
  <li>nieuwe vertalingen maken of vertalingen nalezen</li>
  <li>documentatie maken of verbeteren</li>
  <li><a href="../website">de website helpen onderhouden</a></li>
  <li>helpen bij het verwerken van bugs (door patches aan te leveren, door
      goede bugrapporten in te dienen, door het bestaan van de bug te
      bevestigen, door manieren te zoeken om het probleem te
      reproduceren, ...)</li>
  <li>door een actief lid te worden van een verpakkingsteam (bijv. debian-qt-kde
      of debian-gnome)</li>
  <li>door een actief lid te worden van een subproject (bijv. debian-installer
      of debian-desktop)</li>
  <li>enz.</li>
</ul>

<p>De <a href="$(DOC)/developers-reference/new-maintainer.html">Referentiehandleiding voor ontwikkelaars van Debian</a>
(Debian Developer's Reference) bevat verschillende concrete suggesties voor het
uitvoeren van verschillende van deze taken (in het bijzonder hoe u
bereidwillige sponsors kunt vinden).</p>

<p>Het proces voor nieuwe leden van Debian is het proces waarbij men een
officiële ontwikkelaar van Debian (Debian Developer - DD) wordt. Dit is de
traditionele functie met volledig lidmaatschap. Een DD kan deelnemen aan
verkiezingen in Debian. DD's met uploadrechten kunnen om het even welk pakket
uploaden naar het archief. Voor u zich kandidaat stelt om een DD met
uploadrechten te worden, moet u minstens een staat van dienst van zes maanden
in het onderhouden van pakketten hebben. Dit kan bijvoorbeeld bestaan uit het
uploaden van pakketten als <a href="https://wiki.debian.org/DebianMaintainer">onderhouder
van Debian</a> (Debian Maintainer - DM), het samenwerken binnen een team of het
onderhouden van pakketten die door sponsors geüpload worden. DD's zonder
uploadrechten (non-uploading DDs) hebben in het archief dezelfde rechten als
onderhouders van Debian (Debian Maintainers). Voor u zich kandidaat stelt om
een DD zonder uploadrechten te worden, moet u een zichtbare en significante
staat van dienst hebben binnen het project.</p>

<p>Het is belangrijk om te begrijpen dat het proces voor nieuwe leden deel
uitmaakt van de inspanningen van Debian voor kwaliteitsverzekering. Het vinden
van ontwikkelaars die voldoende tijd kunnen besteden aan hun taken binnen
Debian, is moeilijk en daarom vinden we het belangrijk om te controleren dat
kandidaten hun werk kunnen volhouden en het goed doen. Dit is de reden voor de
vereiste dat toekomstige leden reeds enige tijd actief geëngageerd moeten zijn
in Debian.</p>

<p><a name="developer-priveleges"></a>Elke ontwikkelaar van Debian:</p>
<ul>
  <li>is lid van het Debian project;</li>
  <li>mag een stem uitbrengen over zaken die het hele project betreffen;</li>
  <li>kan inloggen op de meeste systemen die Debian draaiende houden;</li>
  <li>heeft uploadrechten voor <em>alle</em> pakketten
   (behalve ontwikkelaars zonder uploadrechten, die de rechten van
   een DM hebben);</li>
  <li>heeft toegang tot de mailinglijst debian-private.</li>
</ul>

<p>Met andere woorden, als u een Debian-ontwikkelaar wordt, krijgt u
verschillende belangrijke rechten met betrekking tot de infrastructuur van het
project. Dit vergt uiteraard veel vertrouwen in en engagement van de
kandidaat.</p>

<p>Bijgevolg is het hele proces voor nieuwe leden erg streng en grondig. Dit is
niet bedoeld om mensen die geïnteresseerd zijn om een geregistreerde
ontwikkelaar te worden, te ontmoedigen, maar het verklaart waarom het proces
voor nieuwe leden zo veel tijd in beslag neemt.</p>

<p>Lees eerst de <a href="#Glossary">Definities uit de vakwoordenlijst</a> voor
u de rest van de pagina's leest.</p>

<p>De volgende pagina's zullen van belang zijn voor kandidaten:</p>

<ul>
 <li><a href="nm-checklist">Checklist - vereiste stappen voor kandidaten</a>
  <ul>
   <li><a href="nm-step1">Step 1: Kandidatuur</a></li>
   <li><a href="nm-step2">Step 2: Identificatie</a></li>
   <li><a href="nm-step3">Step 3: Filosofie en procedures</a></li>
   <li><a href="nm-step4">Step 4: Taken en vaardigheden</a></li>
   <li><a href="nm-step5">Step 5: Aanbeveling</a></li>
   <li><a href="nm-step6">Step 6: Controle door de frontdesk</a></li>
   <li><a href="nm-step7">Step 7: Controle door de Debian Account Manager en het creëren van een account</a></li>
  </ul></li>
 <li><a href="https://nm.debian.org/public/newnm">Aanmeldingsformulier</a></li>
</ul>

<p>Indien u een ontwikkelaar van Debian bent en geïnteresseerd bent in deelname
aan het proces voor nieuwe leden, ga dan naar deze pagina's:</p>
<ul>
  <li><a href="nm-amchecklist">Checklist voor kandidatuurbeheerders</a></li>
  <li><a href="nm-advocate">Pleitbezorger zijn voor een toekomstig lid</a></li>
  <li><a href="nm-amhowto">Mini-HOWTO voor kandidatuurbeheerders</a></li>
  <li><a href="$(HOME)/events/keysigning">Mini-HOWTO over ondertekenen van sleutels</a></li>
</ul>

<p>Varia:</p>
<ul>
  <li><a href="https://nm.debian.org/">Statusdatabase voor het nieuwe-ledenproces</a></li>
  <li><a href="https://nm.debian.org/process/">Lijst met huidige kandidaten</a></li>
  <li><a href="https://nm.debian.org/public/managers">Lijst met huidige kandidatuurbeheerders</a></li>
</ul>

<define-tag email>&lt;<a href="mailto:%0">%0</a>&gt;</define-tag>

<h2><a name="Glossary">Definities uit de vakwoordenlijst</a></h2>
<dl>
 <dt><a name="Advocate">Pleitbezorger</a>:</dt>
  <dd>Een <a href="#Member">lid van Debian</a> dat pleit voor de kandidaat. Men
   moet de <a href="#Applicant">kandidaat</a> behoorlijk goed kennen en in
   staat zijn een overzicht te geven van het werk van de kandidaat, diens
   interesses en plannen.
   Een pleitbezorgers is vaak de <a href="#Sponsor">sponsor</a> van de
   kandidaat.
  </dd>

 <dt><a name="Applicant">Kandidaat</a>, nieuw lid, vroeger ook nieuwe
  onderhouder (New Maintainer - NM):</dt>
  <dd>Een persoon die het lidmaatschap van Debian aanvraagt als ontwikkelaar
      van Debian.</dd>

 <dt><a name="AppMan">Kandidatuurbeheerder</a> (Application Manager - AM):</dt>
  <dd>Een <a href="#Member">lid van Debian</a> dat toegewezen werd aan een <a
   href="#Applicant">kandidaat</a> om de informatie te verzamelen die de
   <a href="#DAM">Debian Account Managers</a> nodig hebben om een beslissing te
   kunnen nemen over een kandidaatstelling. Eenzelfde kandidatuurbeheerder kan
   toegewezen worden aan meer dan één kandidaat.</dd>

 <dt><a name="DAM">Debian Account Manager</a> (DAM): <email da-manager@debian.org></dt>
  <dd>Een <a href="#Member">lid van Debian</a> dat gemachtigd werd door de
   projectleider van Debian (DPL) om het aanmaken en verwijderen van accounts
   in Debian te beheren. De DAM's hebben de eindbeslissing over een
   kandidatuur.</dd>

 <dt><a name="FrontDesk">Frontdesk</a>: <email nm@debian.org></dt>
  <dd>De leden van de frontdesk doen het infrastructurele werk voor het
   NM-proces, zoals het in ontvangst nemen van nieuwe kandidaturen, van
   pleitbezorgingen en van de eindrapporten over de kandidaturen en het
   toewijzen van AM's aan NM's. Zij zijn het aanspreekpunt indien er zich
   problemen voordoen in verband met een kandidatuur.</dd>

 <dt><a name="Member">Lid, ontwikkelaar</a>:</dt>
  <dd>Een lid van Debian, dat het proces voor nieuwe leden doorlopen heeft en
   wiens kandidatuur aanvaard werd.</dd>

 <dt><a name="Sponsor">Sponsor</a>:</dt>
  <dd>Een <a href="#Member">lid van Debian</a> dat fungeert als mentor van een
   kandidaat: de sponsor controleert pakketten die door de kandidaat verstrekt
   worden, helpt problemen te vinden en helpt bij het verbeteren van het
   verpakkingswerk. Als de sponsor tevreden is over het pakket, uploadt deze
   het namens de kandidaat naar het archief van Debian. De kandidaat wordt
   geregistreerd als de onderhouder van een dergelijk pakket, ondanks het feit
   dat deze niet zelf het pakket uploadt.</dd>
</dl>
