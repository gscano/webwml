<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various minor issues have been addressed in the GLib library. GLib is a
useful general-purpose C library used by projects such as GTK+, GIMP,
and GNOME.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16428">CVE-2018-16428</a>

    <p>In GNOME GLib, g_markup_parse_context_end_parse() in gmarkup.c
    had a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16429">CVE-2018-16429</a>

    <p>GNOME GLib had an out-of-bounds read vulnerability in
    g_markup_parse_context_parse() in gmarkup.c, related to utf8_str().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13012">CVE-2019-13012</a>

    <p>The keyfile settings backend in GNOME GLib (aka glib2.0) before
    created directories using g_file_make_directory_with_parents
    (kfsb->dir, NULL, NULL) and files using g_file_replace_contents
    (kfsb->file, contents, length, NULL, FALSE,
    G_FILE_CREATE_REPLACE_DESTINATION, NULL, NULL, NULL). Consequently,
    it did not properly restrict directory (and file) permissions.
    Instead, for directories, 0777 permissions were used; for files,
    default file permissions were used. This issue is similar to
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-12450">CVE-2019-12450</a>.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.42.1-1+deb8u2.</p>

<p>We recommend that you upgrade your glib2.0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1866.data"
# $Id: $
