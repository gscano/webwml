<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several flaws have been found in ruby2.1, an interpreter of an
object-oriented scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15845">CVE-2019-15845</a>

      <p>Path matching might pass in File.fnmatch and File.fnmatch? due
      to a NUL character injection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16201">CVE-2019-16201</a>

      <p>A loop caused by a wrong regular expression could lead to a denial
      of service of a WEBrick service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16254">CVE-2019-16254</a>

      <p>This is the same issue as <a href="https://security-tracker.debian.org/tracker/CVE-2017-17742">CVE-2017-17742</a>, whose fix was not complete.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16255">CVE-2019-16255</a>

      <p>Giving untrusted data to the first argument of Shell#[] and
      Shell#test might lead to a code injection vulnerability.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.5-2+deb8u8.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2007.data"
# $Id: $
