<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3142">CVE-2017-3142</a>

     <p>An attacker who is able to send and receive messages to an authoritative
     DNS server and who has knowledge of a valid TSIG key name may be able to
     circumvent TSIG authentication of AXFR requests via a carefully constructed
     request packet. A server that relies solely on TSIG keys for protection
     with no other ACL protection could be manipulated into:</p>
    <ul>
     <li>providing an AXFR of a zone to an unauthorized recipient</li>
     <li>accepting bogus NOTIFY packets</li>
    </ul></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3143">CVE-2017-3143</a>

     <p>An attacker who is able to send and receive messages to an authoritative
     DNS server and who has knowledge of a valid TSIG key name for the zone and
     service being targeted may be able to manipulate BIND into accepting an
     unauthorized dynamic update.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u17.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1025.data"
# $Id: $
