<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10790">CVE-2017-10790</a>

      <p>The _asn1_check_identifier function in GNU Libtasn1 through 4.12
      causes a NULL pointer dereference and crash when reading crafted
      input that triggers assignment of a NULL value within an asn1_node
      structure. It may lead to a remote denial of service attack.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.13-2+deb7u5.</p>

<p>We recommend that you upgrade your libtasn1-3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1038.data"
# $Id: $
