<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This is a follow-up update for the recently discovered -dSAFER issues
reported by Tavis Ormandy.</p>

<p>Tavis Ormandy discovered multiple vulnerabilites in Ghostscript, an
interpreter for the PostScript language, which could result in denial of
service, the creation of files or the execution of arbitrary code if a
malformed Postscript file is processed (despite the dSAFER sandbox being
enabled).</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
9.06~dfsg-2+deb8u11.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1552.data"
# $Id: $
