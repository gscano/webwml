<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in SimpleSAMLphp, a
framework for authentication, primarily via the SAML protocol.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9814">CVE-2016-9814</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9955">CVE-2016-9955</a>

    <p>An incorrect check of return values in the signature validation
    utilities allowed an attacker to get invalid signatures accepted
    as valid in the rare case of an error occurring during validation.</p></li>

<li>SSPSA-201802-01 (no CVE yet)

    <p>Critical signature validation vulnerability.</p></li>

</ul>

<p>In addition this update adds a patch to solve excessive resource
consumption in case of SimpleSAMLphp processing a large metadata file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.2-1+deb7u3.</p>

<p>We recommend that you upgrade your simplesamlphp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1297.data"
# $Id: $
