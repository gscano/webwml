<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the emergency logging system in 389-ds-base (the
389 Directory Server) is affected by a race condition caused by the
invalidation of the concurrently used log file file descriptor without
proper locking. This issue might be triggered by remote attackers to
cause DoS (crash) and any other undefined behavior.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3.5-4+deb8u3.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1526.data"
# $Id: $
