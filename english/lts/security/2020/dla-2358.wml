<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were found in the OpenEXR image library, which could result in denial of service and potentially the execution of arbitrary code when processing malformed EXR image files.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.2.0-11+deb9u1.</p>

<p>We recommend that you upgrade your openexr packages.</p>

<p>For the detailed security status of openexr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openexr">https://security-tracker.debian.org/tracker/openexr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2358.data"
# $Id: $
