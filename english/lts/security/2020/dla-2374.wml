<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue around revealing passwords in the
<tt>gnome-shell</tt> component of the GNOME desktop.</p>

<p>In certain configurations, when logging out of an account the password box
from the login dialog could reappear with the password visible in
cleartext.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17489">CVE-2020-17489</a>

    <p>An issue was discovered in certain configurations of GNOME gnome-shell
    through 3.36.4. When logging out of an account, the password box from the
    login dialog reappears with the password still visible. If the user had
    decided to have the password shown in cleartext at login time, it is then
    visible for a brief moment upon a logout. (If the password were never shown
    in cleartext, only the password length is revealed.)</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3.22.3-3+deb9u1.</p>

<p>We recommend that you upgrade your gnome-shell packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2374.data"
# $Id: $
