<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Tomcat8 is configured with the JMX Remote Lifecycle Listener, a local
attacker without access to the Tomcat process or configuration files
is able to manipulate the RMI registry to perform a man-in-the-middle
attack to capture user names and passwords used to access the JMX
interface. The attacker can then use these credentials to access the
JMX interface and gain complete control over the Tomcat instance.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
8.0.14-1+deb8u16.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2155.data"
# $Id: $
