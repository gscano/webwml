<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Apache Tomcat from 8.5.0 to 8.5.59 could
re-use an HTTP request header value from the previous stream
received on an HTTP/2 connection for the request associated with
the subsequent stream. While this would most likely lead to an
error and the closure of the HTTP/2 connection, it is possible
that information could leak between requests.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
8.5.54-0+deb9u5.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2495.data"
# $Id: $
