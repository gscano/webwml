<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27918">CVE-2020-27918</a>

    <p>Liu Long discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29623">CVE-2020-29623</a>

    <p>Simon Hunt discovered that users may be unable to fully delete
    their browsing history under some circumstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1765">CVE-2021-1765</a>

    <p>Eliya Stein discovered that maliciously crafted web content may
    violate iframe sandboxing policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1789">CVE-2021-1789</a>

    <p>@S0rryMybad discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1799">CVE-2021-1799</a>

    <p>Gregory Vishnepolsky, Ben Seri and Samy Kamkar discovered that a
    malicious website may be able to access restricted ports on
    arbitrary servers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1801">CVE-2021-1801</a>

    <p>Eliya Stein discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1870">CVE-2021-1870</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.30.6-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4877.data"
# $Id: $
