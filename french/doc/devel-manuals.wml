#use wml::debian::ddp title="Manuels pour les développeurs Debian"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="78f856f0bff4125a68adb590b66952d97f183227" maintainer="Jean-Paul Guillonneau"

# Translators
# Mickael Simon, 2001-2003.
# Frédéric Bothamy, 2005-2007.
# Guillaume Delacour, 2009.
# David Prévot, 2010-2012, 2014.
# Jean-Paul Guillonneau 2015-2020

<document "La Charte Debian" "policy">

<div class="centerblock">
<p>
  Ce manuel est la charte de la distribution Debian GNU/Linux. Il aborde
  la structure et le contenu d'une archive Debian, certaines questions
  liées à la conception du système d'exploitation et les exigences
  techniques que chaque paquet doit satisfaire afin d'être inclus dans
  la distribution.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p>
  <a href="https://bugs.debian.org/debian-policy">Modifications
  proposées</a> à la Charte
  </p>

  <p>Documentation supplémentaire pour la Charte&nbsp;:</p>
  <ul>
    <li>la <a href="packaging-manuals/fhs/fhs-3.0.html">norme de la hiérarchie
    des systèmes de fichiers (Filesystem Hierarchy Standard)</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">texte brut</a>]
    <li><a href="debian-policy/upgrading-checklist.html">liste de mise à niveau</a>
    <li>la <a href="packaging-manuals/virtual-package-names-list.yaml">liste
    des paquets virtuels</a>
    <li>les <a href="packaging-manuals/menu-policy/">règles pour Menu</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">texte brut</a>]
    <li>les <a href="packaging-manuals/perl-policy/">règles pour Perl</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">texte brut</a>]
    <li>les <a href="packaging-manuals/debconf_specification.html">spécifications debconf</a>
    <li>les <a href="packaging-manuals/debian-emacs-policy">règles pour Emacsen</a>
    <li>les <a href="packaging-manuals/java-policy/">règles pour Java</a>
    <li>les <a href="packaging-manuals/python-policy/">règles pour Python</a>
    <li>les <a href="packaging-manuals/copyright-format/1.0/">spécifications de copyright</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Référence du développeur Debian" "devref">

<div class="centerblock">
<p>
  Ce manuel décrit les procédures et les ressources pour les responsables
  Debian. Il décrit comment devenir un nouveau développeur, la procédure
  d'envoi des paquets, comment manipuler notre système de suivi des bogues,
  les listes de diffusions, le serveur Internet, etc.

  <p>Ce manuel a été conçu comme un <em>manuel de référence</em> pour tous les
  développeurs Debian, qu'ils soient néophytes ou confirmés.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guide pour les responsables de paquet Debian" "debmake-doc">

<div class="centerblock">
<p>
Ce tutoriel décrit la construction de paquet Debian pour les utilisateurs
Debian et les développeurs potentiels, en utilisant la commande
<code>debmake</code>.
</p>
<p>
Il se concentre sur le nouveau style d’empaquetage et est fourni avec de
nombreux exemples simples.
</p>
<ul>
<li>empaquetage de script d’interpréteur de commandes POSIX</li>
<li>empaquetage de script Python3</li>
<li>C avec Makefile/Autotools/CMake</li>
<li>divers paquets binaires avec des bibliothèques partagées, etc.</li>
</ul>
<p>
Ce « Guide pour les responsables de paquet Debian » peut être considéré comme le
successeur du « Guide pour les nouveaux responsables Debian ».
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Guide des nouveaux responsables Debian" "maint-guide">

<div class="centerblock">
<p>
  Ce document explique la construction d'un paquet Debian GNU/Linux pour
  les utilisateurs Debian de base (et pour les développeurs en
  devenir) dans un langage ordinaire et avec des exemples.

  <p>Contrairement aux tentatives initiales, il est basé sur
  <code>debhelper</code> et les nouveaux outils disponibles pour les
  responsables. L'auteur fait tout son possible pour incorporer et unifier les
  efforts précédents.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  remplacement futur par le "Guide pour les responsables de paquet Debian" (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introduction à l'empaquetage Debian" "packaging-tutorial">

<div class="centerblock">
<p>
Ce tutoriel est une introduction à l'empaquetage Debian.

Il enseigne aux futurs développeurs comment modifier
des paquets existants, comment créer leurs propres
paquets, et comment interagir avec la communauté Debian.

En plus du tutoriel principal, trois sessions
de mise en œuvre sont disponibles.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>


<document "Le système de menu Debian" "menu">

<div class="centerblock">
<p>
  Ce manuel décrit le système de menu Debian et le paquet
  <strong>menu</strong>.

  <p>Le paquet menu est inspiré de install-fvwm2-menu de l'antique
  paquet fvwm2. Cependant, menu essaie de fournir une interface plus générale
  pour la construction de menus. Avec la commande update-menus de ce paquet,
  les paquets n'ont plus besoin d'être modifiés pour chaque nouveau
  gestionnaire de fenêtres X. De plus, il fournit une interface unifiée pour les
  programmes orientés texte comme pour ceux orientés X.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">en ligne</a>
  </availability>
</doctable>
</div>

<hr>

<document "À l’intérieur de l’installateur Debian" "d-i-internals">

<div class="centerblock">
<p>
Ce document a pour but de rendre l’installateur Debian
accessible aux nouveaux développeurs en
privilégiant l’accès à la documentation technique.

<doctable>
  <authors "Frans Pop">
  <maintainer "Équipe de l’installateur Debian">
  <status>
  prêt
  </status>
  <availability>
 <p><a href="https://d-i.debian.org/doc/internals/">HTML en ligne</a></p>
 <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">\
 Sources XML DocBook en ligne</a></p>
  </availability>
</doctable>
</div>
<hr>

<document "dbconfig-common documentation" "dbconfig-common">

<div class="centerblock">
<p>
  Ce document est destiné aux responsables de paquet qui tiennent à jour des
  paquets nécessitant une base de données. Au lieu de mettre en œuvre leur
  propre logique, ils peuvent s’appuyer sur dbconfig-common pour poser les bonnes
  questions lors de l’installation, la mise à niveau, la reconfiguration et la
  désinstallation, et pour créer et remplir la base de données.

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  prêt
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  De plus, le <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">document de conception</a> est aussi disponible.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  Une pratique recommandée pour les paquets dépendant d’une base de données.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  ébauche
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>

