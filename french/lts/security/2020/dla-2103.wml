#use wml::debian::translation-check translation="dff6d1cfaa683ba251d6d1667346d5fee2432cd9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>debian-security-support, le vérificateur de couverture de sécurité de Debian,
a été mis à jour dans jessie-security.</p>

<p>Cela marque la fin de vie du paquet libqb dans Jessie. Une vulnérabilité
récemment rapportée à l’encontre de libqb, permettant aux utilisateurs d’écraser
des fichiers arbitraires à l'aide d'une attaque par lien symbolique, ne peut
être corrigée de manière adéquate dans libqb dans Jessie. L’amont ne prend plus
en charge cette version et aucun paquet dans Jessie ne dépend de libqb.</p>

<p>Nous vous recommandons si vos systèmes ou vos applications dépendent du paquet
libqb fourni par l’archive Debian, de mettre à niveau vos systèmes vers une
publication plus récente de Debian ou de trouver une source alternative et plus
à jour de paquets libqb.</p>

<p>De plus, MySQL 5.5 n’est plus prise en charge. L’amont a mis fin à sa prise
en charge et nous ne pouvons plus rétroporter les correctifs pour de nouvelles
versions à cause d’un manque de détails des correctifs. Les options sont de
basculer vers MariaDB 10.0 dans Jessie ou vers une nouvelle version de MySQL
dans une publication plus récente de Debian.</p>

<p> Pour Debian 8 <q>Lenny</q>, ces problèmes ont été résolus dans la
version 2019.12.12~deb8u2</p> de debian-security-support

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2103.data"
# $Id: $
