#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le système de traitement
d’image GraphicsMagick.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20184">CVE-2018-20184</a>

<p>La fonction WriteTGAImage (tga.c) est sujette à un dépassement de tampon basé
sur le tas. Des attaquants distants peuvent exploiter cette vulnérabilité pour
provoquer un déni de service à l'aide d’un fichier d’image contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20185">CVE-2018-20185</a>

<p>La fonction ReadBMPImage (bmp.c) est sujette à une lecture hors limites de
tampon basé sur le tas. Des attaquants distants peuvent exploiter cette
vulnérabilité pour provoquer un déni de service à l'aide d’un fichier d’image
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20189">CVE-2018-20189</a>

<p>La fonction ReadDIBImage (coders/dib.c) est sujette à une erreur d’assertion.
Des attaquants distants peuvent exploiter cette vulnérabilité pour provoquer
un déni de service à l'aide d’un fichier d’image contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.3.20-3+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1619.data"
# $Id: $
