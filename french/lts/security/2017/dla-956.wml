#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8361">CVE-2017-8361</a>

<p>La fonction flac_buffer_copy (flac.c) dans libsndfile 1.0.28 permet à des
attaquants distants de provoquer un déni de service (dépassement de tampon et
plantage d’application) ou d’avoir un autre impact à l'aide d'un fichier audio
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8363">CVE-2017-8363</a>

<p>La fonction flac_buffer_copy (flac.c) dans libsndfile 1.0.28 permet à des
attaquants distants de provoquer un déni de service (lecture non valable et
plantage d’application) à l'aide d'un fichier audio contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8363">CVE-2017-8363</a>

<p>La fonction flac_buffer_copy dans flac.c dans libsndfile 1.0.28 permet à des
attaquants distants de provoquer un déni de service (lecture hors limites de
tampon basé sur le tas et plantage d'application) à l'aide d'un fichier audio
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8365">CVE-2017-8365</a>

<p>La fonction i2les_array dans pcm.c dans libsndfile 1.0.28 permet à des
attaquants distants de provoquer un déni de service (lecture excessive de tampon
et plantage d'application) à l'aide d'un fichier audio contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.0.25-9.1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsndfile.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-956.data"
# $Id: $
