#use wml::debian::template title="왜 데비안을 사용하는가" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Sebul"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">사용자를 위한 데비안</a></li>
    <li><a href="#devel">개발자를 위한 데비안</a></li>
    <li><a href="#enterprise">기업환경을 위한 데비안</a></li>
  </ul>
</div>

<p>
사용자, 개발자, 기업 환경 등 운영 체제로 데비안을 선택하는 데는 많은 까닭이 있습니다. 
대부분의 사용자는 패키지 및 전체 배포의 안정성과 원활한 업그레이드 프로세스를 인정합니다. 
데비안은 또한 널리 알려져 있습니다.
수많은 아키텍처와 장치에서 실행되며 공개 버그 추적기 및 기타 도구를 제공하기 때문에 소프트웨어 및 하드웨어 개발자들이 사용합니다.
개발자를 위한 전문 환경에서 데비안을 사용할 계획이라면 LTS 버전 및 클라우드 이미지와 같은 추가 이점이 있습니다.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> For me it's the perfect level of ease of use and stability. I've used various different distributions over the years but Debian is the only one that just works. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx?utm_source=share&utm_medium=web2x&context=3">NorhamsFinest on Reddit</a></p>
</aside>

<h2><a id="users">사용자를 위한 데비안</a></h2>

<p>
<dl>
  <dt><strong>데비안은 자유 소프트웨어 입니다.</strong></dt>
  <dd>
  데비안은 자유 오픈 소스 소프트웨어로 만들며 항상 100% <a href="free">free</a>입니다. 
  누구나 사용, 수정 및 배포할 수 있습니다.
  이것이 <a href="../users">사용자</a>에게 약속하는 주요 사항입니다.
  가격도 무료입니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안정적이고 안전합니다.</strong></dt>
  <dd>
  데비안은 랩톱, 데스크톱 및 서버를 포함한 광범위한 장치를 위한 리눅스 기반 운영 체제입니다. 
  뿐만 아니라 모든 패키지에 대해 합리적인 기본 구성을 제공하며 패키지 수명 동안 정기적인 보안 업데이트도 제공합니다.  
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 넓은 하드웨어를 지원합니다.</strong></dt>
  <dd>
  대부분의 하드웨어를 리눅스 커널에서 이미 지원합니다. 
  하드웨어 전용 드라이버는 무료 소프트웨어가 충분하지 않을 때 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 유연한 설치프로그램을 제공합니다.</strong></dt>
  <dd><a
  href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">라이브
  CD</a>는 설치하기 전에 데비안을 시도해 보고 싶은 모든 사람을 위한 것입니다. 
  또한 라이브 시스템에서 데비안을 쉽게 설치할 수 있도록 해주는 Calamares 설치 프로그램도 포함되어 있습니다. 
  더 경험 많은 사용자는 자동화된 네트워크 설치 도구를 사용할 수 있는 가능성을 포함하여 
  세부 조정을 위한 더 많은 옵션과 함께 데비안 설치 프로그램을 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 부드러운 업그레이드를 지원합니다.</strong></dt>
  <dd>
  데비안은 릴리스 주기 내에 쉽고 매끄럽게 업그레이드되는 것으로 잘 알려져 있으며, 
  다음 주요 릴리스로 업그레이드할 수도 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 다른 많은 배포판의 기반입니다.</strong></dt>
  <dd>
  Ubuntu, Knoppix, PureOS, SteamOS 또는 Tails 같이 가장 많이 사용되는 리눅스 배포판 중 상당수는 소프트웨어 기반으로 데비안을 선택합니다. 
  데비안은 모든 도구를 제공하므로 누구든지 필요에 따라 데비안 아카이브의 소프트웨어 패키지를 자신의 패키지로 확장할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안 프로젝트는 커뮤니티입니다.</strong></dt>
  <dd>
  데비안은 리눅스 운영체제일 뿐만이 아닙니다. 
이 소프트웨어는 전 세계 수백 명의 자원봉사자가 함께 만들었습니다. 
프로그래머나 sysadmin이 아니더라도 데비안 커뮤니티 일원이 될 수 있습니다. 
데비안은 공동체적이고 합의 지향적이며 <a href="../devel/constitution">민주적인 통치 구조</a>를 가집니다. 
모든 데비안의 개발자들은 동등한 권리를 가지고 있기 때문에 그것을 단일 회사가 통제할 수 없습니다. 
60개 이상의 국가에 개발자가 있으며 80개 이상의 언어로 데비안 설치관리자 번역을 지원합니다.
  </dd>
</dl>
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> 
The reason behind Debian’s status as a developer’s operating system is the large number of packages and software support, which are important for developers. It’s highly recommended for advanced programmers and system administrators. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma on Fossbytes</a></p>
</aside>

<h2><a id="devel">개발자를 위한 데비안</a></h2>

<p>
<dl>
  <dt><strong>다중 하드웨어 아키텍처</strong></dt>
  <dd>
  amd64, i386, ARM 및 MIPS의 여러 버전, POWER7, POWER8, IBM System z, RISC-V를 포함한 <a href="../ports">긴 CPU 아키텍처 목록</a> 지원. 
  데비안은 구식 및 특정 틈새 아키텍처에서도 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>IoT 및 임베디드 장비</strong></dt>
  <dd>
라즈베리 파이, QNAP의 변형, 모바일 장치, 홈 라우터 및 많은 SBC(싱글 보드 컴퓨터) 같은 광범위한 장치를 지원합니다.
  </dd>
</dl>

<dl>
  <dt><strong>수많은 소프트웨어 패키지</strong></dt>
  <dd>
  데비안에는 가장 많은 수의 설치된 패키지(현재 <packages_in_stable> 개)가 있습니다. <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    형식</a>.
  </dd>
</dl>

<dl>
  <dt><strong>다른 릴리스</strong></dt>
  <dd>
  안정적인 릴리스 외에도 테스트 또는 불안정한 릴리스를 사용하여 새 소프트웨어 버전을 설치할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>공개 버그 추적 시스템</strong></dt>
  <dd>
  데비안 <a href="../Bugs">버그 추적 시스템</a> (BTS)은 웹 브라우저를 통해 누구나 공개적으로 사용할 수 있습니다. 
  우리는 소프트웨어 버그를 숨기지 않으며 새 버그 보고서를 쉽게 제출할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안 정책 및 개발자 도구</strong></dt>
  <dd>
  데비안은 고품질 소프트웨어를 제공합니다. 
  우리 표준에 대해 자세히 알아보려면 배포에 포함된 모든 패키지에 대한 기술 요구 사항을 정의하는 <a 
  href="../doc/debian-policy/">정책 </a>을 읽으십시오. 
  지속적 통합 전략에는 Autopkgtest(패키지에서 테스트 실행), 
  Piuparts(테스트 설치, 업그레이드 및 제거) 및 
  Lintian(패키지 불일치 및 오류 확인)이 있습니다.
  </dd>
</dl> 
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stability is synonym with Debian. [...]  Security is one of the most important Debian features. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis on pontikis.net</a></p>
</aside>

<h2><a id="enterprise">기업환경을 위한 데비안</a></h2>

<dl>
  <dt><strong>데비안은 믿을 수 있습니다.</strong></dt>
  <dd>
  데비안은 단일 사용자 노트북에서 수퍼 콜리더, 증권거래소 및 자동차 산업에 이르는 수천 개의 실제 시나리오에서 매일 신뢰성을 입증하고 있습니다.
그것은 또한 학계와 과학계와 공공부문에서도 인기가 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 전문가가 많습니다.</strong></dt>
  <dd>
  당사의 패키지 관리자들은 데비안 패키징과 새로운 업스트림 버전을 통합하는 것뿐만 아니라 종종 그들은 업스트림 소프트웨어의 전문가이며 업스트림 개발에 직접 기여합니다.
  때때로 그들은 업스트림의 일부이기도 합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안전합니다.</strong></dt>
  <dd>
  데비안은 안정 릴리스를 위해 보안 지원을 받고 있습니다. 
  다른 많은 유통 및 보안 연구자들은 데비안의 보안 추적기에 의존하고 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>장기 지원</strong></dt>
  <dd>
<a href="https://wiki.debian.org/LTS">장기 지원 (LTS)</a> 무료. 
이를 통해 안정적인 릴리스에 대한 지원이 5년 이상 연장됩니다.
또한 제한된 패키지 세트에 대한 지원을 5년 이상 연장하는 <a href="https://wiki.debian.org/LTS/Extended">확장 
LTS</a> 이니셔티브도 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>클라우드 이미지.</strong></dt>
  <dd>
  공식 클라우드 이미지는 모든 주요 클라우드 플랫폼에서 쓸 수 있습니다. 
  또한 맞춤형 클라우드 이미지를 구축 할 수 있도록 도구와 구성도 제공합니다. 
  데스크톱 또는 컨테이너의 가상 머신에서도 데비안을 쓸 수 있습니다.
  </dd>
</dl>
</p>
