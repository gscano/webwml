#use wml::debian::template title="Dicas de tradução do Debian-Installer"
#use wml::debian::translation-check translation="b8ba05460f524de3235e89b67719c02da5e0f5d0"

<h3>Codificação de caracteres</h3>

<p>Você deve se assegurar de que está usando a codificação de caracteres
apropriada. Se não usar uma codificação apropriada, sua tradução não vai
construir. A codificação padrão para documentos XML é UTF-8. Se quiser
utilizar uma codificação de caracteres diferente, você terá que especificar
isso explicitamente no preâmbulo do XML.</p>

<p>No caso de você querer usar uma codificação diferente, você terá que
adicionar a seguinte linha como a primeira linha de cada documento
traduzido:</p>

<pre>
   &lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
</pre>

<p>Neste caso, foi assumido que você quer usar ISO-8859-1 como a
codificação de caracteres. Use um valor diferente se usar uma
codificação diferente. Você pode examinar as traduções francesa (fr) e
holandesa (nl) como exemplos.</p>

<p>De qualquer forma, a codificação de caracteres preferida é UTF-8. Um
exemplo disso é a tradução grega (el).</p>

<h3>Diversos</h3>

<p>Para verificar mais facilmente as alterações dos documentos originais
em inglês, por favor substitua as seguintes linhas:</p>

<pre>
   &lt;!-- retain these comments for translator revision tracking --&gt;
   &lt;!-- &#036;Id: welcome.xml 12756 2004-04-06 22:23:56Z fjpop-guest &#036; --&gt;
</pre>

<p>pela seguinte linha</p>

<pre>
   &lt;!-- original version: 12756 --&gt;
</pre>

<p>em cada documento que você traduzir. O número na nova linha
deve ser o mesmo que o número de revisão no arquivo original que
você traduziu.</p>
